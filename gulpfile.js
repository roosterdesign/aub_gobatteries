'use strict';
 
var gulp = require('gulp'),
	less = require('gulp-less'),
	path = require('path'),
	cssmin = require('gulp-cssmin'),
  combineMq = require('gulp-combine-mq'),
  plumber = require('gulp-plumber'),
  notify = require("gulp-notify"),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename');

  var onError = function (err) {  
    notify.onError({
          title:    "Gulp",
          subtitle: "Failure!",
          message:  "Error: <%= error.message %>",
          sound:    "Beep"
    })(err);
    this.emit('end');
  };


// LESS
gulp.task('less', ['less:child']);
gulp.task('less:child', function() {
  return gulp.src('./wp-content/themes/gobatteries/css/src/style.less')
    .pipe(plumber({errorHandler: onError}))
    .pipe(less({ paths: [ path.join(__dirname, 'less', 'includes') ] }))
    .pipe(combineMq())
    .pipe(cssmin())
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./wp-content/themes/gobatteries/'));
});


// Scripts
gulp.task('scripts', ['scripts:child']);
gulp.task('scripts:child', function() {
  gulp.src([
      './wp-content/themes/gobatteries/js/src/plugins/jquery.easing.js',
      './wp-content/themes/gobatteries/js/src/plugins/owl.carousel.js',
      './wp-content/themes/gobatteries/js/src/main.js'])
    .pipe(plumber({errorHandler: onError}))
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./wp-content/themes/gobatteries/js/dist/'))
});


// Watch/Default Tasks
gulp.task('watch', function () {
  gulp.watch('./wp-content/themes/gobatteries/css/src/**/*.less', ['less:child']);
  gulp.watch('./wp-content/themes/gobatteries/js/src/**/*.js', ['scripts:child']);  
});
gulp.task('default', ['watch', 'less', 'scripts']);