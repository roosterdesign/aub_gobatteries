$(document).ready(function(){

	// Mobile Nav Dropdown
	(function mobileNavDropdown() {

		// Main Nav
		$('.nav-toggle').click(function() {
			$this = $(this)
			if ( $this.hasClass('open') ) {
				$('.nav-wrap nav').stop().slideUp(850, 'easeInOutExpo');
				$this.removeClass('open');
			} else {
				$('.nav-wrap nav').stop().slideDown(850, 'easeInOutExpo');
				$this.addClass('open');				
			}
		});

		// Subnav
		$('nav li.has-dropdown span').click(function() {
			$this = $(this)			
			if ( $this.parents('li').hasClass('open') ) {
				$this.siblings('ul').stop().slideUp(850, 'easeInOutExpo');
				$this.parents('li').removeClass('open');
			} else {
				$this.siblings('ul').stop().slideDown(850, 'easeInOutExpo');
				$this.parents('li').addClass('open');				
			}
		});

	})();


	// Header Search Mobile Toggle
	(function mobileHeaderSearch(){
		$('.search-toggle').click(function() {
			$this = $(this)
			if ( $this.hasClass('open') ) {
				$('.nav-wrap .widget_product_search').stop().slideUp(850, 'easeInOutExpo');
				$this.removeClass('open');
			} else {
				$('.nav-wrap .widget_product_search').stop().slideDown(850, 'easeInOutExpo');
				$this.addClass('open');
			}
		});
	})();


	// Battery Lookup
	(function batteryLookupSearch(){

		// Swap forms
		$('#battery-lookup .by-reg').click(function(){
			$(this).hide();
			$('#battery-lookup form.reg-vin').hide();
			$('#battery-lookup .by-vin').show();
			$('#battery-lookup form.reg-number').show();

		});

		$('#battery-lookup .by-vin').click(function(){
			$(this).hide();
			$('#battery-lookup form.reg-number').hide();
			$('#battery-lookup .by-reg').show();
			$('#battery-lookup form.reg-vin').show();
		});

	})();


	// Homepage Hero Carousel
	(function homeCarousel(){
		var owl = $('body.home .hero .owl-carousel');

		owl.owlCarousel({
		    loop: true,
		    mouseDrag: true,
		    smartSpeed: 1000,
		    autoplay: true,
		    autoplayTimeout: 5000,
		    autoplayHoverPause: true,
		    items: 1	    
		});

		$("body.home .hero .next").click(function(){
			console.log('clicked');
			owl.trigger('next.owl.carousel');
		});

		$("body.home .hero .prev").click(function(){
			console.log('clicked');
			owl.trigger('prev.owl.carousel');
		});

	})();


	// Manufacturer Logos Carousel
	(function manufacturerCarousel(){
		var owl = $('.manufacturers .owl-carousel');

		owl.owlCarousel({
		    loop: true,
		    mouseDrag: true,
		    smartSpeed: 1000,
		    autoplay: true,
		    autoplayTimeout: 5000,
		    autoplayHoverPause: true,
		    responsive : {
			    0 : {
			        items: 1
			    },
			    768 : {
			        items: 4
			    },
			    992 : {
			        items: 5
			    }
			}  
		});

		$(".manufacturers .next").click(function() {
			owl.trigger('next.owl.carousel');
		});

		$(".manufacturers .prev").click(function() {
			owl.trigger('prev.owl.carousel');
		});

	})();


	// Cheat Hero Title on Product Detail Page
	(function singleProductHeroText(){
		if ( $('body').hasClass('single-product')) {
			var $currentProdCat = $('.woocommerce-breadcrumb a:last-child').text();
			$('body.single-product .hero h1').text($currentProdCat);
		};
	})();


	// Convert terminal text into image
	// $(".shop_attributes th:contains('Terminals')").siblings('td').find('p').each(function() {
	// 	$url = $(this).text();
	// 	$(this).replaceWith( "<img src="+$url+" />" );
	// });

	$(".shop_attributes th").filter(function() {
		// Matches exact string
		return $(this).text() === "Terminals";
		}).siblings('td').find('p').each(function() {
			$url = $(this).text();
			$(this).replaceWith( "<img src="+$url+" class='terminal-diagram' />" );
		});

});


// Google Map
function initMap() {
	var $myLatLng = {lat: 52.287448, lng: -1.598031};
	var map = new google.maps.Map(document.getElementById('map'), {
		center: $myLatLng,
		scrollwheel: false,
		zoom: 14,
	});
  	var image = new google.maps.MarkerImage('/wp-content/themes/gobatteries/img/map-marker.png', null, null, null, new google.maps.Size(55,80));
    var marker = new google.maps.Marker({
      position: $myLatLng,
      map: map,
      icon: image,
      title: 'Go Batteries'
    });
	 google.maps.event.addDomListener(window, 'resize', function() {
	    map.setCenter($myLatLng);
	});
};