<?php if (!isset($parentCatId)) { $parentCatId = 6; } ?>
<div class="product-categories">
	<h3>Product Categories</h3>
	<?php $product_categories = get_terms( 'product_cat', array('hide_empty' => 0, 'parent' => $parentCatId, 'taxonomy' => 'product_cat', 'hierarchical' => 1, 'show_option_none' => '' ));
		 echo '<ul class="child-categories">';
			 foreach ( $product_categories as $product_category ) :
				echo '<li><a href="' . get_term_link( $product_category->slug, $product_category->taxonomy ) . '">' . $product_category->name . '</a></li>';
			 endforeach; 
		 echo '</ul>'; wp_reset_postdata(); ?>	
</div>