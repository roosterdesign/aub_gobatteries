<?php global $trimmedAssetPath; ?>
<div id="battery-lookup">
	<h2>Battery Lookup</h2>
	<img src="<?php echo $trimmedAssetPath; ?>/img/battery-lookup/vehicle-icons.png" width="200" height="24" class="vehicle-icons">
	<p>Enter your Motorcycle, Car or Light Van VIN/Chassis No. or browse models.</p>

	<form action="/battery-lookup" method="post" class="reg-number">
		<input type="text" name="reg" placeholder="Enter reg">
		<div class="submit"><input type="submit" value=""></div>
	</form>

	<form action="/battery-lookup" method="post" class="reg-vin">
		<input type="text" name="reg" placeholder="VIN/Chasis No">
		<div class="submit"><input type="submit" value=""></div>
	</form>

	<a class="btn btn-white">By Make / Model</a>
	<a class="btn btn-grey by-vin">By VIN / Chassis No.</a>
	<a class="btn btn-grey by-reg">By Registration</a>

</div>