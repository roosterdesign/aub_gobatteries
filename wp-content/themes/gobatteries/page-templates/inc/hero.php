<?php if ( $image ) : $image = $image; else: $image = $trimmedAssetPath.'/img/home-hero-fallback.jpg'; endif; ?>
<div class="hero" style="background-image:url(<?php echo $image; ?>)">
	<div class="container">
		<h1><?php echo $title; ?></h1>
	</div>
</div>