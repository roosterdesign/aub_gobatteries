<section class="manufacturers">
	<div class="container">
	<div class="owlnav"><span class="prev"></span><span class="next"></span></div>
		<?php $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 1, 'parent' => 7, 'taxonomy' => 'product_cat' ); $subcats = get_categories($args);
    	echo '<div class="owl-carousel">';
			foreach ($subcats as $sc) :
	        	$link = get_term_link( $sc->slug, $sc->taxonomy );
				if ( get_field('carousel_image', 'product_cat_' .  $sc->term_id) ) :
					$image = wp_get_attachment_image( get_field('carousel_image', 'product_cat_' .  $sc->term_id), 'manufacturer_thumbnail' );
				else:
					$image = '<img src="'.$trimmedAssetPath.'img/fallback/manufacturer-thumbnail.gif">';
				endif; ?>
				<a href="<?php echo $link;?> " class="item"><?php echo $image; ?></a>
	      <?php endforeach;
		echo '</div>'; wp_reset_postdata(); ?>
	</div>
</section>