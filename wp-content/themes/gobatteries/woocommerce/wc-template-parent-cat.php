<?php global $trimmedAssetPath; ?>
<?php $title = get_queried_object()->name; $queried_object = get_queried_object(); $image = wp_get_attachment_image_src( get_field('hero_image', $queried_object), 'hero' ); $image = $image[0]; include(get_stylesheet_directory() . "/page-templates/inc/hero.php"); ?>
<div id="parent-category" class="container main">
	<?php woocommerce_breadcrumb(); ?>
	 <?php echo category_description(); ?>
	<div class="category-list">
		<?php $currentCategory = get_queried_object()->term_id; $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 1, 'parent' => $currentCategory, 'taxonomy' => 'product_cat'); $subcats = get_categories($args); 
	    echo '<ul class="wooc_sclist">';
			foreach ($subcats as $sc) :
			$link = get_term_link( $sc->slug, $sc->taxonomy );
		        if ( get_woocommerce_term_meta( $sc->term_id, 'thumbnail_id', true ) ) :		        	
		        	$image = wp_get_attachment_image_src( get_woocommerce_term_meta( $sc->term_id, 'thumbnail_id', true ), 'category_thumbnail' )['0'];
				else:
					$image = $trimmedAssetPath.'img/fallback/category-thumbnail.gif';
				endif; ?>
				<?php $desc = get_field('short_description', 'product_cat_' . $sc->term_id); ?>
					<li>
						<a href="<?php echo $link; ?>" style="background-image:url(<?php echo $image; ?>)" class="thumbnail"><div><p><?php echo $sc->name; ?></p></div></a>
						<?php if ($desc) : ?>
							<p class="description"><?php trim_content($desc, 145); ?></p>
						<?php else: ?>
							<p class="description">Please click below to view our range of <?php echo $sc->name; ?> products.</p>
						<?php endif; ?>						
						<a href="<?php echo $link; ?>" class="btn">More info</a>
					</li>
			<?php endforeach; echo '</ul>'; wp_reset_postdata(); ?>
	</div>
</div>
<?php include(get_stylesheet_directory() . "/page-templates/inc/manufacturer-carousel.php"); ?>