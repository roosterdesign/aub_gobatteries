<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) : exit;  endif; ?>
<?php get_header(); ?>
<?php 
 	if (is_tax('product_cat')) :
        $cat = get_term(get_queried_object_id(), 'product_cat');
        if ( empty($cat->parent) ) :
        	wc_get_template( 'wc-template-parent-cat.php' );
        else:
        	wc_get_template( 'wc-template-child-cat.php' );
        endif;
    endif;
?>
<?php get_footer(); ?>