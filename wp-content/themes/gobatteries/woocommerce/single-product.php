<?php get_header(); ?>
<?php $title = ''; $image = wp_get_attachment_image_src( get_field('hero_image'), 'hero' ); $image = $image[0]; include(get_stylesheet_directory() . "/page-templates/inc/hero.php"); ?>
	<div class="main">
		<div class="container">
			<?php woocommerce_breadcrumb(); ?>
			<div class="main-col">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php wc_get_template_part( 'content', 'single-product' ); ?>
				<?php endwhile; ?>
			</div>
			<aside class="sidebar">				
				<div class="mini-cart">
					<h2>Cart</h2>
					<?php woocommerce_mini_cart(); ?>
				</div>
				<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
				<?php include(get_stylesheet_directory() . "/page-templates/inc/sidebar-prod-categories.php"); ?>				
			</aside>
		</div>
	</div>
<?php get_footer(); ?>