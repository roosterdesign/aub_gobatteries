<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colours
$bg              = get_option( 'woocommerce_email_background_color' );
$body            = get_option( 'woocommerce_email_body_background_color' );
$base            = get_option( 'woocommerce_email_base_color' );
$base_text       = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text            = get_option( 'woocommerce_email_text_color' );

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>
#wrapper {
	/*background-color: <?php echo esc_attr( $bg ); ?>;*/
	margin: 0;
	padding: 70px 0 70px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	/*box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important;*/
	background-color: <?php echo esc_attr( $body ); ?>;
	/*border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;*/
	/*border-radius: 3px !important;*/
}

#email_header {
	background-color: #ffffff;
	color: <?php echo esc_attr( $text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	padding: 0 0 30px 0;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#email_header p {
	margin: 0;
}

#email_header p span {
	color: #92BF1F;
	position: relative;
	top: -1px;
}

#email_header img {
	height: auto;
	display: block;
	width: 230px;
}

#template_header {
	background-color: #92BF1F;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: #ffffff;
	font-weight: bold;
	margin: 0;
}

#template_footer {
	background: #464646;
	color: #ffffff !important;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	padding: 20px 20px 20px 20px;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_footer p {
	color: #ffffff;
	font-weight: bold;
}

#template_footer p span.tel {
	display: inline-block;
	padding: 0 50px 0 0;
}

#template_footer p span.tel img {
	float: left;
	margin-top: -1px;
	display: inline-block;
}

#template_footer p span.email {
	display: inline-block;
}

#template_footer p span.email img {
	float: left;
	margin-top: -1px;
	display: inline-block;		
}

#template_footer td {
	color: #ffffff;
	font-weight: bold;
	padding: 0;
	-webkit-border-radius: 6px;
}

#template_footer td a {
	color: #ffffff;
	text-decoration: none;
	font-weight: bold;
}

#template_footer #credit {
	/*border:0;
	color: <?php echo esc_attr( $base_lighter_40 ); ?>;
	font-family: Arial;
	font-size:12px;
	line-height:125%;
	padding: 0 0 30px 0;*/

	font-size:18px;
	text-align:center;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
	padding: 30px 0;
}

#body_content table td td {
	padding: 12px;
}

#body_content table td thead th {
	background: #92BF1F;
	border: none;
	color: #ffffff;
	padding: 12px;
}

#body_content table td tfoot th {
	border-right: none !important;
	text-align: right !important;
}

#body_content table td tfoot td {
	border-left: none !important;
	text-align: right !important;
}




#body_content p {
	margin: 0 0 16px;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $base ); ?>;
}

#header_wrapper {
	padding: 10px 20px;
	display: block;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 26px;
	font-weight: bold;
	line-height: 150%;
	margin: 0 0 10px 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
	/*text-shadow: 0 1px 0 <?php echo esc_attr( $base_lighter_20 ); ?>;*/
	-webkit-font-smoothing: antialiased;
}

h2 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 18px;
	font-weight: bold;
	line-height: 130%;
	margin: 22px 0 10px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 130%;
	margin: 22px 0 10px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $base ); ?>;
	font-weight: normal;
	text-decoration: none;
}

img {
	border: none;
	display: inline;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	line-height: 100%;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
}

ul {
	margin: 0 0 20px 0;
	padding: 0;
}

ul li {
	list-style: none;
	margin: 0;
	padding: 0;
}

#body_content table#addresses td {
	border: none !important;
	padding: 0; !important;
}

#body_content table#addresses td h3 {
	margin-top: 0;
}

<?php
