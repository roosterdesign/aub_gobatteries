<?php global $trimmedAssetPath; $tel_number = get_option("tel_number"); ?>
<?php $title = get_queried_object()->name; $queried_object = get_queried_object(); $image = wp_get_attachment_image_src( get_field('hero_image', $queried_object), 'hero' ); $image = $image[0]; include(get_stylesheet_directory() . "/page-templates/inc/hero.php"); ?>
<div id="child-category" class="container main">
	<?php woocommerce_breadcrumb(); ?>
	<div class="main-col">
		<?php echo category_description(); ?>
		<h2>To find out which brand best suits both your battery needs and your budget call our team today on <?php echo $tel_number;?></h2>
		<?php if ( have_posts() ) : ?>			
		<div class="view-all">
			<h3>Start shopping for <?php echo get_queried_object()->name; ?></h3>

			<?php global $wp_query; $per_page = $wp_query->get( 'posts_per_page' ); $total = $wp_query->found_posts;

				if ( !($total <= $per_page || -1 === $per_page ) ) : ?>
					<?php if (is_paged()) : ?> 
						<a href="../../?view=all" class="btn">View All <?php echo get_queried_object()->name; ?></a>
					<?php else: ?>
						<a href="?view=all" class="btn">View All <?php echo get_queried_object()->name; ?></a>
					<?php endif; ?>
				<?php endif; ?>

				<?php if( $_GET['view'] === 'all' ) : ?>
					<a href="." class="btn">View Less <?php echo get_queried_object()->name; ?></a>
				<?php endif; ?>

		</div>
			<?php do_action( 'woocommerce_before_shop_loop' ); ?>
			<?php woocommerce_product_loop_start(); ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php wc_get_template_part( 'content', 'product' ); ?>
				<?php endwhile; ?>
			<?php woocommerce_product_loop_end(); ?>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
			<br><br>
			<?php wc_get_template( 'loop/no-products-found.php' ); ?>
		<?php endif; ?>
	</div>
	<aside class="sidebar">
		<?php $parentCatId = get_queried_object()->parent; ?>
		<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
		<?php include(get_stylesheet_directory() . "/page-templates/inc/sidebar-prod-categories.php"); ?>
	</aside>
</div>
<?php include(get_stylesheet_directory() . "/page-templates/inc/manufacturer-carousel.php"); ?>