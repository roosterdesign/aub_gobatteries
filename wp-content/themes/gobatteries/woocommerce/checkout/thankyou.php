<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$tel_number = get_option("tel_number");
$email = get_option("email");

?>

<div class="main-col thank-you">
	
	<?php if ( $order ) : ?>

		<div class="row">
			<div class="col-md-6">
				<?php if ( $order->has_status( 'failed' ) ) : ?>
					<p class="woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>
					<p class="woocommerce-thankyou-order-failed-actions"> <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a> <?php if ( is_user_logged_in() ) : ?><a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a><?php endif; ?></p>
				<?php else : ?>			
					<!-- <p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Your order has been received.', 'woocommerce' ), $order ); ?></p> -->
					<h1>Your order has been received.</h1>
					<p>We will be in touch soon with more details about your order and delivery times.</p>
					<p>Please feel free to contact us if you have any questions about your purchase.</p>
					<p class="large"><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:<?php echo $tel_number; ?>" class="tel"><?php echo $tel_number; ?></a><br>
					<i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
				<?php endif; ?>
			</div>
			<div class="col-md-6">
				<img src="/wp-content/uploads/2017/01/thank-you.jpg" alt="Thank You" class="thank-you-img">
			</div>
		</div>		

	<?php else : ?>

		<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

	<div class="popular-products">

	<h2>Popular Products</h2>

	<?php $query_args = array(
	        'post_type' => 'product',
	        'post_status' => 'publish',
	        'ignore_sticky_posts' => 1,
	        'posts_per_page' => '4',
	        'columns' => '4',
	        'fields' => 'ids',
	        'meta_key' => 'total_sales',
	        'orderby' => 'meta_value_num',
	        'meta_query' => WC()->query->get_meta_query() );
	    $best_sell_products_query = new WP_Query($query_args);
		if ( $best_sell_products_query->have_posts() ) : ?>				
				<ul class="products">
					<?php while ( $best_sell_products_query->have_posts() ) : $best_sell_products_query->the_post(); ?>
						<li <?php post_class(); ?>>
							<?php
							do_action( 'woocommerce_before_shop_loop_item' );
							do_action( 'woocommerce_before_shop_loop_item_title' );
							do_action( 'woocommerce_shop_loop_item_title' );
							do_action( 'woocommerce_after_shop_loop_item_title' );
							do_action( 'woocommerce_after_shop_loop_item' );
							?>
						</li>
					<?php endwhile;  ?>
				</ul>
			<?php endif; ?>
		<?php wp_reset_postdata(); ?>

	</div>

</div>

<aside class="sidebar">
	<?php $parentCatId = get_queried_object()->parent; ?>
	<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
	<?php include(get_stylesheet_directory() . "/page-templates/inc/sidebar-prod-categories.php"); ?>
</aside>