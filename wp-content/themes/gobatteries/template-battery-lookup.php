<?php
/*
Template Name: Battery Lookup Page
*/
if ( !function_exists( 'wc_get_product_id_by_sku' ) ) { 
    require_once '/includes/wc-product-functions.php'; 
}

/* FOR TEST PURPOSTS ONLY */
$searchResult = $_POST["reg"];
if ( $searchResult == '' ) {
	$sku = 'TEST001';
} else {
	$sku = 'G3500UK';
}
/* end */

$result = wc_get_product_id_by_sku($sku); 
$url = get_permalink( $result );
header('Location: '.$url);
?>