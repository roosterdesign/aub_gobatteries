<?php $tel_number = get_option("tel_number"); ?>
<footer id="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4 about">
				<h2>About Go Batteries</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate eum numquam recusandae earum assumenda consequuntur inventore porro, reprehenderit quasi.</p>
				<p>Dicta eius, fugit dolorem, incidunt voluptatum deleniti voluptatem necessitatibus tempore expedita. Harum consectetur laudantium, vitae odio, saepe architecto amet. Dolore a in commodi, atque omnis, molestiae sunt cum numquam autem consequatur!</p>
				<h3>Call us on - <a href="tel:<?php echo $tel_number; ?>" class="tel"><?php echo $tel_number; ?></a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, delectus.</p>
			</div>
			<div class="col-sm-6 col-md-4 top-rated-products">
				<h2>Top Rated Products</h2>

				<?php $query_args = array(
					'posts_per_page' => 3,
					'no_found_rows'  => 1,
					'post_status'    => 'publish',
					'post_type'      => 'product',
					'meta_key'       => '_wc_average_rating',
					'orderby'        => 'meta_value_num',
					'order'          => 'DESC',
					'meta_query'     => WC()->query->get_meta_query(),
					'tax_query'      => WC()->query->get_tax_query(),
				);
				$r = new WP_Query( $query_args ); if ( $r->have_posts() ) : ?>
				<ul>
					<?php while ( $r->have_posts() ) :
						$r->the_post();
						wc_get_template( 'woocommerce/footer-top-rated-products.php', array( 'show_rating' => true ) );
					endwhile; ?>
				</ul>
				<?php endif; wp_reset_postdata(); ?>
			</div>
			<div class="col-sm-6 col-md-4">
				<h2>Do you need some help?</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nesciunt esse saepe aliquam vitae est qui minus voluptates aperiam neque!</p>
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false" tabindex=32]'); ?>
			</div>
		</div>
		
	</div>

	<div class="bottom-footer">
		<div class="container">
			<p class="copyright">&copy; Go Batteries Ltd. All rights reserved.</p>
			<p>Website and digital marketing by <a href="http://www.auburn.co.uk/" target="_blank">Auburn Creative Ltd</a></p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
