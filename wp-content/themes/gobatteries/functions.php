<?php 

	define('MY_WORDPRESS_FOLDER',$_SERVER['DOCUMENT_ROOT']);
	define('MY_THEME_FOLDER',str_replace('\\','/',dirname(__FILE__)));
	define('MY_THEME_PATH','/' . substr(MY_THEME_FOLDER,stripos(MY_THEME_FOLDER,'wp-content'))); 

	require( get_stylesheet_directory() . '/functions/clean-head.php' );
	require( get_stylesheet_directory() . '/functions/login.php' );
	require( get_stylesheet_directory() . '/functions/menu-items.php' );
	require( get_stylesheet_directory() . '/functions/navigation.php' );
	require( get_stylesheet_directory() . '/functions/site-settings.php' );
	require( get_stylesheet_directory() . '/functions/image-settings.php' );
	require( get_stylesheet_directory() . '/functions/custom-post-types.php' );
	require( get_stylesheet_directory() . '/functions/comments.php' );
	require( get_stylesheet_directory() . '/functions/general-helpers.php' );
	require( get_stylesheet_directory() . '/functions/dashboard.php' );
	require( get_stylesheet_directory() . '/functions/shortcodes.php' );
	require( get_stylesheet_directory() . '/functions/sidebars.php' );
	require( get_stylesheet_directory() . '/functions/enqueue.php' );
	require( get_stylesheet_directory() . '/functions/shop.php' );

	// REMOVE STOREFRONT INLINE CSS
	function my_theme_remove_storefront_standard_functionality() {
		set_theme_mod('storefront_styles', '');
		set_theme_mod('storefront_woocommerce_styles', '');  
	}
	add_action( 'init', 'my_theme_remove_storefront_standard_functionality');

	// REMOVE WP EMOJI
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// REMOVE PRIMARY FROM CATEGORY
	// add_filter( 'wpseo_primary_term_taxonomies', '__return_false' );

	// Fix Page Category tree
	function taxonomy_checklist_checked_ontop_filter ($args) {
	    $args['checked_ontop'] = false;
	    return $args;
	  }
	add_filter('wp_terms_checklist_args','taxonomy_checklist_checked_ontop_filter');


	// Editor Styles
	add_action( 'init', 'cd_add_editor_styles' );
	function cd_add_editor_styles() {
	    add_editor_style( 'css/editor-styles.css' );
	}


	// Custom Editor Styles
	add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
	function fb_mce_editor_buttons( $buttons ) {
	    $value = array_search( 'formatselect', $buttons );
		if ( FALSE !== $value ) {
		    foreach ( $buttons as $key => $value ) {
		        if ( 'formatselect' === $value )
		            unset( $buttons[$key] );
		    }
		}
		array_unshift( $buttons, 'styleselect' );
	    return $buttons;
	}

	add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );

	function fb_mce_before_init( $settings ) {

	     $style_formats = array(
	     	array(
	            'title' => 'Paragraph',
	            'items' => array(
		            array(
		                'title' => 'Intro Paragraph',
		                'selector' => 'p',
		                'classes' => 'headline'
		            )
	            )
            ),
	        array(
	            'title' => 'Headers',
	                'items' => array(
	                array(
	                    'title' => 'Header 1',
	                    'format' => 'h1'
	                ),
	                array(
	                    'title' => 'Header 2',
	                    'format' => 'h2'
	                ),
	                array(
	                    'title' => 'Header 3',
	                    'format' => 'h3'
	                ),
	                array(
	                    'title' => 'Header 4',
	                    'format' => 'h4'
	                ),
	                array(
	                    'title' => 'Header 5',
	                    'format' => 'h5'
	                )
	            )
	        ),
	        array(
	            'title' => 'Buttons',
	                'items' => array(
	                array(
	                    'title' => 'Large Grey Button',
	                    'selector' => 'a',
	                    'classes' => 'btn btn-lrg btn-grey'
	                )
	            )
	        ),
	    );

	    $settings['style_formats'] = json_encode( $style_formats );
	    return $settings;
	}


	// Gravity Forms
	// add_filter("gform_confirmation_anchor", create_function("","return true;"));
	add_filter( 'gform_confirmation_anchor', '__return_true' );

	add_filter("gform_init_scripts_footer", "init_scripts");
		function init_scripts() {
		return true;
	}


	/**
	* Preview WooCommerce Emails.
	* @author WordImpress.com
	* @url https://github.com/WordImpress/woocommerce-preview-emails
	* If you are using a child-theme, then use get_stylesheet_directory() instead
	*/

	$preview = get_stylesheet_directory() . '/woocommerce/emails/woo-preview-emails.php';

	if(file_exists($preview)) {
	    require $preview;
	}

?>