<?php
/*
Template Name: Homepage
*/
?>
<?php get_header(); ?>
	<div class="hero">
		<div class="owlnav"><div class="container"><span class="prev"></span><span class="next"></span></div></div>
		<div class="container">
			<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
		</div>
		<div class="owl-carousel">
			<?php if( have_rows('carousel') ): while ( have_rows('carousel') ) : the_row(); ?>
					<?php if ( get_sub_field('slide_image') ) :
						$image = wp_get_attachment_image_src( get_sub_field('slide_image'), 'home_hero' );
						$image = $image[0];
					else:
						$image = $trimmedAssetPath.'/img/home-hero-fallback.jpg';						
					endif; ?>
			        <div class="item" style="background-image: url(<?php echo $image;?>)">
						<div class="container">	
							<div class="panel">
								<h2><?php the_sub_field('slide_title'); ?></h2>
			        			<p><?php the_sub_field('slide_body'); ?></p>
							</div>
						</div>
					</div>
		    <?php endwhile; endif; ?>
		</div>
	</div>
	<div class="container category-list">
		<?php if(get_field('browse_range_label')) : ?>
			<h2><?php the_field('browse_range_label'); ?></h2>
		<?php endif; ?>
		<?php $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 0, 'parent' => 6, 'taxonomy' => 'product_cat', 'exclude' => 50 ); $subcats = get_categories($args); 
	    echo '<ul class="wooc_sclist">';
			foreach ($subcats as $sc) :
			$link = get_term_link( $sc->slug, $sc->taxonomy );
		        if ( get_woocommerce_term_meta( $sc->term_id, 'thumbnail_id', true ) ) :		        	
		        	$image = wp_get_attachment_image_src( get_woocommerce_term_meta( $sc->term_id, 'thumbnail_id', true ), 'category_thumbnail' )['0'];
				else:
					$image = $trimmedAssetPath.'img/fallback/category-thumbnail.gif';
				endif;
          		echo '<li><a href="'. $link .'" style="background-image:url('. $image .')" class="thumbnail"><div><p>'.$sc->name.'</p></div></a></li>';
			endforeach;
	    echo '</ul>'; wp_reset_postdata(); ?>
	</div>
	<?php include(get_stylesheet_directory() . "/page-templates/inc/manufacturer-carousel.php"); ?>
<?php get_footer(); ?>