<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<div>
		<label class="screen-reader-text" for="s"><?php _e( 'Search', 'woocommerce' ); ?></label>
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="search-field" placeholder="<?php _e( 'Search', 'woocommerce' ); ?>" />
		<input type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Search' ); ?>" />
		<input type="hidden" name="post_type" value="product" />
	</div>
</form> 