<?php get_header(); ?>


<?php $title = 'Page Not Found'; $image = wp_get_attachment_image_src( get_field('hero_image'), 'hero' ); $image = $image[0]; include(get_stylesheet_directory() . "/page-templates/inc/hero.php"); ?>


<div class="container main">

<?php woocommerce_breadcrumb(); ?>

	<div class="main-col">
		<p>It looks like nothing was found at this location. Please <a href="/">click here</a> to return to the homepage.</p>
	</div>

	<aside class="sidebar">
		<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
		<?php include(get_stylesheet_directory() . "/page-templates/inc/sidebar-prod-categories.php"); ?>
	</aside>
	
</div>

<?php include(get_stylesheet_directory() . "/page-templates/inc/manufacturer-carousel.php"); ?>

<?php get_footer(); ?>