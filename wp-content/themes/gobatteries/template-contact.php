<?php
/*
Template Name: Contact Page
*/
?>
<?php get_header(); ?>
<?php $title = get_the_title(); $image = wp_get_attachment_image_src( get_field('hero_image'), 'hero' ); $image = $image[0]; include(get_stylesheet_directory() . "/page-templates/inc/hero.php"); ?>
	<div class="container main">
		<?php woocommerce_breadcrumb(); ?>
		<div class="main-col">
			<div class="row">
				<div class="col-md-6">
					<?php the_content(); ?>
				</div>
				<div class="col-md-6">
					<h2>Ask us a question?</h2>
					<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
				</div>
			</div>
			<div id="map"></div>
		</div>
		<aside class="sidebar">
			<?php include(get_stylesheet_directory() . "/page-templates/inc/battery-lookup.php"); ?>
			<?php include(get_stylesheet_directory() . "/page-templates/inc/sidebar-prod-categories.php"); ?>
		</aside>		
	</div>
<?php get_footer(); ?>