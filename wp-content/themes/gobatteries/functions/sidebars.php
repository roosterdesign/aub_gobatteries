<?php 



/*--- REGISTER SIDEBARS ---*/

if ( function_exists('register_sidebar') ) {
  register_sidebar(array(
    //'before_widget' => '<li id="%1$s" class="widget %2$s">',
    //'after_widget' => '</li>',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => '</h2>',
  ));
}


if ( function_exists('register_sidebar') ) {
  // register_sidebar(array(
  //   'name' => 'The Bridge Sidebar',
  //   'id' => 'bridge-sidebar',
  //   'description' => 'Custom sidebar for The Bridge pages',
  //   'before_widget' => '<li id="%1$s" class="widget %2$s">',
  //   'after_widget' => '</li>',
  //   'before_title' => '<h2 class="widgettitle">',
  //   'after_title' => '</h2>',
  // ));
}



// deregister all default widgets

function cs_unregister_default_widgets() {
  unregister_widget('WP_Widget_Pages');
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Archives');
  unregister_widget('WP_Widget_Links');
  unregister_widget('WP_Widget_Meta');
  unregister_widget('WP_Widget_Search');
  unregister_widget('WP_Widget_Text');
  unregister_widget('WP_Widget_Categories');
  unregister_widget('WP_Widget_Recent_Posts');
  unregister_widget('WP_Widget_Recent_Comments');
  unregister_widget('WP_Widget_RSS');
  unregister_widget('WP_Widget_Tag_Cloud');
  unregister_widget('WP_Nav_Menu_Widget');
 }
 add_action('widgets_init', 'cs_unregister_default_widgets');




// register custom widgets 

// function cs_register_custom_widgets() {  
//     //register_widget( 'My_Widget' );  
// }  
// add_action( 'widgets_init', 'cs_register_custom_widgets' );




?>