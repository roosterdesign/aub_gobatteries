<?php 
   
  if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' ); 
  }

  if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
  if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'home_hero', '1920', '550', true);
    add_image_size( 'hero', '1920', '335', true);
    add_image_size( 'category_thumbnail', '234', '140', true);
    add_image_size( 'manufacturer_thumbnail', '234', '90', true);
  }

  // function remove_default_image_sizes( $sizes) {
  //     unset( $sizes['thumbnail']);
  //     // unset( $sizes['medium']);
  //     unset( $sizes['medium_large']);
  //     // unset( $sizes['large']);
  //     unset( $sizes['full']);
  //     return $sizes;
  // }
  
  // add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
  add_filter('jpeg_quality', function($arg){return 75;});
  add_filter( 'wp_editor_set_quality', function($arg){return 75;} );

?>