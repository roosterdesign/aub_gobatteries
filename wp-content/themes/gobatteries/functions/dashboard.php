<?php 


// remove wordpress logo 

add_action('admin_head', 'my_custom_logo');

function my_custom_logo() {
   echo '<style type="text/css">
         #wp-admin-bar-wp-logo { display: none; }</style>';
}


// remove dashboard widgets

function example_remove_dashboard_widgets() {
  // Globalize the metaboxes array, this holds all the widgets for wp-admin
  global $wp_meta_boxes;

  // Remove the incomming links widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); 

  // Remove right now
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
}

// Hoook into the 'wp_dashboard_setup' action to register our function
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' );




// add custom widgets

// function example_dashboard_widget_function() {
//   // Display whatever it is you want to show
//   echo "Hello World, I'm a great Dashboard Widget";
// } 

// // Create the function use in the action hook
// function example_add_dashboard_widgets() {
//   wp_add_dashboard_widget('example_dashboard_widget', 'Example Dashboard Widget', 'example_dashboard_widget_function');
// }
// // Hoook into the 'wp_dashboard_setup' action to register our other functions
// add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' );



add_filter('admin_footer_text', 'remove_footer_admin'); //change admin footer text
function remove_footer_admin () {
echo "Web development by Auburn";
}


?>