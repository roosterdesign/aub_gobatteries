<?php
	if (!is_admin()) add_action("wp_enqueue_scripts", "child_script_enqueue", 11);
		function child_script_enqueue() {

			wp_deregister_style('storefront-fonts');

			// wp_deregister_script('storefront-navigation');
			// wp_deregister_script('storefront-skip-link-focus-fix');
			// wp_deregister_style('custom-background');
			
			wp_deregister_script('jquery');
			wp_register_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', false, null, true );
    		wp_enqueue_script( 'jquery' );

    		wp_deregister_script('jquery-migrate');
    		// wp_register_script( 'jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js', ['jquery'], null, true );
    		// wp_enqueue_script( 'jquery-migrate' );

			wp_deregister_script('main');
		    wp_register_script('main', '/wp-content/themes/gobatteries/js/dist/main.min.js', ['jquery'], null, true);
		    wp_enqueue_script('main');

		    if(is_page_template('template-contact.php')) {
          		wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBCkJBy-ntOOMWrXuKYuuR1fQ2Af91G-60&callback=initMap', ['jquery'], null, true);
          		wp_enqueue_script('googlemaps');
      		}

	}
?>