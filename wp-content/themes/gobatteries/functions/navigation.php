<?php

/* REMOVE THE CLASS-ITUS THAT THE STANDARD NAV HAS */

add_filter('nav_menu_css_class','remove_nav_menu_classes');
function remove_nav_menu_classes($classes) {
    $current = array_search('current-menu-item', $classes); 
    return array($classes[1], $classes[$current]); 
}

/*--- ADD FIRST AND LAST CLASSES TO NAVIGATION LIST ITEMS ---*/ 

 function add_first_and_last($output) {
   $output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
   $output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
   $output = str_replace(' class="menu-item "', '', $output);
   $output = str_replace('menu-item "', 'menu-item"', $output);
   $output = str_replace('first-menu-item menu-item', 'first-menu-item', $output);
   $output = str_replace('last-menu-item menu-item', 'last-menu-item', $output);

   // now sort out the links - don't need no absolute bullshit here :) 
   //$output = str_replace(get_bloginfo('url'), '', $output);
   
   return $output;
 }
 add_filter('wp_nav_menu', 'add_first_and_last');


 /*--- REMOVE IDS FROM THE NAVIGATION ARRAYS ---*/ 
  // add_filter('nav_menu_item_id', 'remove_nav_ids');
  // function remove_nav_ids($nav) {
  //   return array();
  // }



//   function my_cool_menu_function(){
//   register_nav_menus( array(
//     'primary' => 'Primary Navigation',
//     'footer' => 'Footer Navigation'
//   ));
// }

// add_action( 'after_setup_theme', 'my_cool_menu_function' );



// wp_nav_menu( array( 'menu' => 'Top Nav', 'container' => '', 'walker' => new subMenu() ) );
// class subMenu extends Walker_Nav_Menu {
//     function start_lvl( &$output, $depth = 0, $args = array() ) {
//         $indent = str_repeat("\t", $depth);
//         $output .= "\n$indent<div class='sub-menu-wrap'><span class='arrow'></span><ul class='sub-menu'>\n";
//     }
//     function end_lvl( &$output, $depth = 0, $args = array() ) {
//         // ob_start();
//         // include(get_template_directory() ."/page-templates/inc/continue-application-button.php");
//         $indent = str_repeat("\t", $depth);
//         $output .= "$indent</ul>";
//         // $output .= "<div class='foot'>";
//         // $output .= ob_get_clean();
//         // $output .= "</div></div>";
//         $output .= "</div>";
//     }
// };


// class footerMenu extends Walker_Nav_Menu {
//   function start_el(&$output, $item, $depth, $args) {
//      global $wp_query;
//      $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
//      $class_names = $value = '';
//      $classes = empty( $item->classes ) ? array() : (array) $item->classes;
//      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
//      $class_names = ' class="'. esc_attr( $class_names ) . '"';
//      $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
//      $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
//      $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
//      $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
//      $attributes .= ! empty( $item->url )        ? ' onclick="trackOutboundLink(\''.$item->url.'\'); return false;"' : '';
//     $item_output = $args->before;
//     $item_output .= '<a'. $attributes .'>';
//     $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
//     $item_output .= $description.$args->link_after;
//     $item_output .= '</a>';
//     $item_output .= $args->after;
//     $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
//   }
// };



?>