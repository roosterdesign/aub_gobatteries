<?php
	
	// Homepage Battery Type Categories
	function woocommerce_subcats_from_parentcat($parent_cat_NAME) {
	  $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
	  $product_cat_ID = $IDbyNAME->term_id;
	    $args = array(
	       'hierarchical' => 1,
	       'show_option_none' => '',
	       'hide_empty' => 0,
	       'parent' => $product_cat_ID,
	       'taxonomy' => 'product_cat'
	    );
	  $subcats = get_categories($args);
	    echo '<ul class="wooc_sclist">';
	      foreach ($subcats as $sc) {
	        $link = get_term_link( $sc->slug, $sc->taxonomy );
			$image = wp_get_attachment_url( get_woocommerce_term_meta( $sc->term_id, 'thumbnail_id', true ) );
          		echo '<li><a href="'. $link .'" style="background-image:url('. $image .')">'.$sc->name;	
				echo '</a></li>';
	      }
	    echo '</ul>';
	}


	// Change in/out of stock message (remove number)
	// add_filter( 'woocommerce_get_availability', 'custom_get_availability', 1, 2);
	// function custom_get_availability( $availability, $_product ) {
	//   global $product;
	//   if ( $_product->is_in_stock() ) $availability['availability'] = __('In Stock', 'woocommerce');
	//   if ( !$_product->is_in_stock() ) $availability['availability'] = __('Sold Out', 'woocommerce');
	//   return $availability;
	// }



	// Remove shop from breadcrumbs trail
	function my_remove_shop_from_breadcrumbs( $trail ) {
	    unset( $trail['shop'] );
	    return $trail;
	}
	add_filter( 'wpex_breadcrumbs_trail', 'my_remove_shop_from_breadcrumbs', 20 );


	// View all products		
	if($_GET['view'] === 'all'){
	    add_filter( 'loop_shop_per_page', create_function( '$cols', 'return -1;' ), 20 );
	} else {
	    add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );
	}


	// Change Pagination Arrows
	add_filter( 'woocommerce_pagination_args', 	'rocket_woo_pagination' );
	function rocket_woo_pagination( $args ) {
		$args['prev_text'] = '<i class="fa fa-angle-left"></i>';
		$args['next_text'] = '<i class="fa fa-angle-right"></i>';
		return $args;
	}


	// Remove Single Product unused tabs
	add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
	function woo_remove_product_tabs( $tabs ) {
	    unset( $tabs['description'] );      	// Remove the description tab
	    // unset( $tabs['reviews'] ); 			// Remove the reviews tab
	    // unset( $tabs['additional_information'] );  	// Remove the additional information tab
	    return $tabs;
	}


	// Rename Single Product Tabs
	add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
	function woo_rename_tabs( $tabs ) {
		// $tabs['description']['title'] = __( 'More Information' );		// Rename the description tab
		// $tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
		$tabs['additional_information']['title'] = __( 'Specification' );	// Rename the additional information tab
		return $tabs;
	}





?>