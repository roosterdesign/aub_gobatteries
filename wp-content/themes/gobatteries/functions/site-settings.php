<?php 


function setup_theme_admin_menus() {  

    add_menu_page('Theme settings', 'Site Settings', 'manage_categories',   
        'hps_theme_settings');  
          
    add_submenu_page('hps_theme_settings',   
        'Front Page Elements', 'Front Page', 'manage_categories',   
        'hps_theme_settings', 'theme_front_page_settings');   
}  
  
// We also need to add the handler function for the top level menu  
function theme_front_page_settings() { 
    $tel_number = get_option("tel_number");
    $email = get_option("email");
    $facebook = get_option("facebook");
    $twitter = get_option("twitter");
    $linkedin = get_option("linkedin");

    if (isset($_POST["update_settings"])) {
        $tel_number = esc_attr($_POST["tel_number"]);     
        update_option("tel_number", $tel_number);
        $email = esc_attr($_POST["email"]);     
        update_option("email", $email);
        $facebook = esc_attr($_POST["facebook"]);     
        update_option("facebook", $facebook);
        $twitter = esc_attr($_POST["twitter"]);     
        update_option("twitter", $twitter);
        $linkedin = esc_attr($_POST["linkedin"]);     
        update_option("linkedin", $linkedin);
  ?>  
    <div id="message" class="updated"><p>Settings saved</p></div>  
  <?php } ?>  

    <div class="wrap">  
        <?php screen_icon('themes'); ?> <h2>General Website Settings</h2>
  
        <form method="POST" action="">  
            <input type="hidden" name="update_settings" value="Y" />  
            <table class="form-table">  
                <!-- <tr><th scope="row" colspan="2"><h3 style="margin-bottom:0;">Top Header Links</h3></th></tr> -->
                <tr valign="top">  
                    <th scope="row">
                        <label for="tel_number">  
                            Telephone Number
                        </label>   
                    </th>  
                    <td>  
                        <input type="text" name="tel_number" value="<?php echo $tel_number; ?>" class="regular-text"/>
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">
                        <label for="email">  
                            Email Address
                        </label>   
                    </th>  
                    <td>  
                        <input type="text" name="email" value="<?php echo $email; ?>" class="regular-text"/>
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">
                        <label for="facebook">  
                            Facebook
                        </label>   
                    </th>  
                    <td>  
                        <input type="text" name="facebook" value="<?php echo $facebook; ?>" class="regular-text"/>
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">
                        <label for="twitter">  
                            Twitter
                        </label>   
                    </th>  
                    <td>  
                        <input type="text" name="twitter" value="<?php echo $twitter; ?>" class="regular-text"/>
                    </td>  
                </tr>
                <tr valign="top">  
                    <th scope="row">
                        <label for="linkedin">  
                            LinkedIn
                        </label>   
                    </th>  
                    <td>  
                        <input type="text" name="linkedin" value="<?php echo $linkedin; ?>" class="regular-text"/>
                    </td>  
                </tr>
            </table>  
            <p>  
                <input type="submit" value="Save settings" class="button-primary"/>  
            </p>
        </form>  
    </div>  
  <?php  
  
}    

add_action("admin_menu", "setup_theme_admin_menus");
$tel_number = get_option("tel_number");
add_action("admin_menu", "setup_theme_admin_menus");
$email = get_option("email");

add_action("admin_menu", "setup_theme_admin_menus");
$facebook = get_option("facebook");
add_action("admin_menu", "setup_theme_admin_menus");
$twitter = get_option("twitter");
add_action("admin_menu", "setup_theme_admin_menus");
$linkedin = get_option("linkedin");



?>