<?php global $trimmedAssetPath;
$tel_number = get_option("tel_number");
$email = get_option("email");

$facebook = get_option("facebook");
$twitter = get_option("twitter");
$linkedin = get_option("linkedin");

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="site-header">

	<div class="top-header">
		<div class="container">
			<ul class="contact">
				<li class="tel">
					<a href="tel:<?php echo $tel_number; ?>"><i class="fa fa-phone" aria-hidden="true"></i><span class="visible-desktop"><?php echo $tel_number; ?></span></a>
				</li>
				<li class="email">
					<a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i><span class="visible-desktop"><?php echo $email; ?></span></a>
				</li>
			</ul>
			<ul class="social">
				<?php if ( $facebook ) : ?>
					<li class="facebook"><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if ( $twitter ) : ?>
					<li class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<?php endif; ?>

				<?php if ( $linkedin ) : ?>
					<li class="linkedin"><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<?php endif; ?>
			</ul>
			<ul class="account">
				<?php if ( is_user_logged_in() ) : ?>
					<li><a href="/my-account/customer-logout"><span>Logout</span><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
					<li><a href="/my-account"><span>My Account</span><i class="fa fa-user" aria-hidden="true"></i></a></li>
				<?php else: ?>
					<li><a href="/my-account"><span>Login / Register</span><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
				<?php endif; ?>				
			</ul>
		</div>
	</div>

	<div class="container">
		<a href="/" class="logo"><img src="<?php echo $trimmedAssetPath; ?>/img/logo.png" width="300" height="70"></a>
		<p class="tagline">Right battery <span>-</span> Right application <span>-</span> Great advice</p>
	</div>

	<div class="nav-wrap">
		<div class="container">

			<i class="fa fa-search search-toggle" aria-hidden="true"></i>


			<div class="nav-toggle">Navigation<i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav>
				<ul>
					<li><a href="/">Home</a></li>
					<li class="has-dropdown">
						<a href="/about-us">About</a><span></span>
						<ul>
							<li><a href="/about-us/customer-service">Customer service</a></li>
							<li><a href="/about-us/terms-and-conditions">Terms and Conditions</a></li>
							<li><a href="/about-us/delivery">Delivery</a></li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="/products/battery-types">Battery types</a><span></span>
						<?php $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 0, 'parent' => 6, 'taxonomy' => 'product_cat' ); $subcats = get_categories($args);
						    echo '<ul>';
								foreach ($subcats as $sc) :							        
					          		echo '<li><a href="'. get_term_link( $sc->slug, $sc->taxonomy ) .'">'.$sc->name.'</a></li>';
								endforeach;
						    echo '</ul>'; wp_reset_postdata(); ?>
					</li>
					<li class="has-dropdown">
						<a href="/products/manufacturers">Manufacturers</a><span></span>
						<?php $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 1, 'parent' => 7, 'taxonomy' => 'product_cat' ); $subcats = get_categories($args);
						    echo '<ul>';
								foreach ($subcats as $sc) :							        
					          		echo '<li><a href="'. get_term_link( $sc->slug, $sc->taxonomy ) .'">'.$sc->name.'</a></li>';
								endforeach;
						    echo '</ul>'; wp_reset_postdata(); ?>
					</li>
					<li class="has-dropdown">
						<a href="/products/battery-chargers">Battery chargers</a><span></span>
						<?php $args = array( 'hierarchical' => 1, 'show_option_none' => '', 'hide_empty' => 0, 'parent' => 21, 'taxonomy' => 'product_cat' ); $subcats = get_categories($args);
						    echo '<ul>';
								foreach ($subcats as $sc) :							        
					          		echo '<li><a href="'. get_term_link( $sc->slug, $sc->taxonomy ) .'">'.$sc->name.'</a></li>';
								endforeach;
						    echo '</ul>'; wp_reset_postdata(); ?>
					</li>
					<li><a href="/contact">Contact</a></li>
				</ul>
			</nav>			
			<?php storefront_header_cart(); ?>
			<?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
			
		</div>	
	</div>

</header>